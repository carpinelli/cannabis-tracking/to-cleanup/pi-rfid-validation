# RFID Reader Input Validation

Validates input scanned in with an RFID reader. The two active modules
are currently used for performing a move audit, and to destroy
extremely large batches of plants.

## Contact

mr.carpinelli@protonmail.ch

## Contributing

This is mostly a personal project, but any contributions are welcome and
appreciated, provided they align with the goals of the project.

## Building



## License

All contributions are made under the [GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html). See the [LICENSE](LICENSE).
