#include "moveaudit.hpp"
#include "inputwindow.hpp"

#include <QApplication>

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  MoveAudit w;
  InputWindow iw;
  w.show();
  iw.show();

  return a.exec();
}
