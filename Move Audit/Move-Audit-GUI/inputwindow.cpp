#include "inputwindow.hpp"
#include "ui_inputwindow.h"

InputWindow::InputWindow(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::InputWindow)
{
  ui->setupUi(this);
}

InputWindow::~InputWindow()
{
  delete ui;
}
