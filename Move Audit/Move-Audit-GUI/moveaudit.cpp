// CREATE CONSTANTS WHERE POSSIBLE
// NOT IN PYTHON!

#include "moveaudit.hpp"
#include "ui_moveaudit.h"

#include <string>
#include <set>

// Input Fields
char roomLetter = 'A';  // 'A' is the first room
unsigned int roomNumber = 4;  // 4 is the first Bloom room
std::string tierNumber = "All";  // Options other than "All": 1-2(4)(5)
unsigned int totalPlants = 0;  // Number of plants in the current room/tier
// time_t moveDate = NULL;  // C++ DATE FOR Move date
std::string currentTag;

// Display Fields
std::string activeRoom;
unsigned int amountScanned = 0;
unsigned int amountRemaining = 0;
// totalPlants
std::string lastTenTags[10];

// A Move Has:
std::set<std::string> tags;  // Set of tags for current room


MoveAudit::MoveAudit(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MoveAudit)
{
  ui->setupUi(this);


  /*
  // Setup the UI
    ui->setupUi(this);
    // Put 0.0 in Display
    ui->Display->setText(QString::number(calcVal));

    // Will hold references to all the number buttons
    QPushButton *numButtons[10];

    // Cycle through locating the buttons
    for(int i = 0; i < 10; ++i)
    {
        QString butName = "Button" + QString::number(i);

        // Get the buttons by name and add to array
        numButtons[i] = Calc::findChild<QPushButton *>(butName);

        // When the button is released call num_pressed()
        connect(numButtons[i], SIGNAL(released()), this,
                SLOT(NumPressed()));
    }

    // Connect signals and slots for math buttons
    connect(ui->Add, SIGNAL(released()), this,
            SLOT(MathButtonPressed()));
    connect(ui->Subtract, SIGNAL(released()), this,
            SLOT(MathButtonPressed()));
    connect(ui->Multiply, SIGNAL(released()), this,
            SLOT(MathButtonPressed()));
    connect(ui->Divide, SIGNAL(released()), this,
            SLOT(MathButtonPressed()));

    // Connect equals button
    connect(ui->Equals, SIGNAL(released()), this,
            SLOT(EqualButtonPressed()));

    // Connect change sign
    connect(ui->ChangeSign, SIGNAL(released()), this,
            SLOT(ChangeNumberSign()));
    */
}

MoveAudit::~MoveAudit()
{
  delete ui;
}

/*
void Calc::NumPressed()
{
    // Sender returns a pointer to the button pressed
    QPushButton *button = (QPushButton *)sender();

    // Get number on button
    QString butVal = button->text();
    // Get the value in the display
    QString displayVal = ui->Display->text();

    if((displayVal.toDouble() == 0) || (displayVal.toDouble() == 0.0))
    {
        // calcVal = butVal.toDouble();
        ui->Display->setText(butVal);
    }

    else
    {
        // Put the new number to the right of whats there
        QString newVal = displayVal + butVal;

        // Double version of number
        double dblNewVal = newVal.toDouble();

        // calcVal = newVal.toDouble();

        // Set value in display and allow up to 16
        // digits before using exponents
        ui->Display->setText(QString::number(dblNewVal, 'g', 16));
    }
}

void Calc::MathButtonPressed()
{
    // Cancel out previous math button clicks
    divTrigger = false;
    multTrigger = false;
    addTrigger = false;
    subTrigger = false;

    // Store current value in Display
    QString displayVal = ui->Display->text();
    calcVal = displayVal.toDouble();

    // Sender returns a pointer to the button pressed
    QPushButton *button = (QPushButton *)sender();

    // Get math symbol on the button
    QString butVal = button->text();

    if(QString::compare(butVal, "/", Qt::CaseInsensitive) == 0)
    {
        divTrigger = true;
    }

    else if(QString::compare(butVal, "*", Qt::CaseInsensitive) == 0)
    {
        multTrigger = true;
    }

    else if(QString::compare(butVal, "+", Qt::CaseInsensitive) == 0)
    {
        addTrigger = true;
    }

    else
    {
        subTrigger = true;
    }

    // Clear display
    ui->Display->setText("");
}

void Calc::EqualButtonPressed()
{
    // Holds new calculation
    double solution = 0.0;

    // Get value in display
    QString displayVal = ui->Display->text();
    double dblDisplayVal = displayVal.toDouble();

    // Make sure a math button was pressed
    if(addTrigger || subTrigger || multTrigger || divTrigger)
    {
        if(addTrigger)
        {
            solution = calcVal + dblDisplayVal;
        }

	else if(subTrigger)
	{
	    solution = calcVal - dblDisplayVal;
	}

	else if(multTrigger)
	{
	    solution = calcVal * dblDisplayVal;
	}

	else
	{
	    solution = calcVal / dblDisplayVal;
	}
    }

    // Put solution in display
    ui->Display->setText(QString::number(solution));
}

void Calc::ChangeNumberSign()
{
    // Get the value in the display
    QString displayVal = ui->Display->text();

    // Regular expression checks if it is a number
    // plus sign
    QRegExp reg("[-+]?[0-9.]*");

    // If it is a number change the sign
    if(reg.exactMatch(displayVal))
    {
        double dblDisplayVal = displayVal.toDouble();
        double dblDisplayValSign = -1 * dblDisplayVal;

        // Put solution in display
        ui->Display->setText(QString::number(dblDisplayValSign));
    }
}
*/
